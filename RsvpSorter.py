import datetime as dt
import json
import requests as req
import time


# Get the streaming data from meetup and place in a dict for more efficient data parsing
def get_data_stream(time_period: int = 60):
    rsvp_data = []
    end_time = time.time() + time_period
    # Request docs suggest following syntax ('with -> as') for managing streaming data
    with req.get('http://stream.meetup.com/2/rsvps', stream=True) as rsvp_raw_data:
        current_time = time.time()
        for rsvp in rsvp_raw_data.iter_lines():
            if rsvp:
                # Decode is necessary here otherwise the rsvp will be appended as a byte object
                rsvp_data.append(json.loads(rsvp.decode('utf-8')))
            if time.time() > end_time:
                break
    return rsvp_data


# Below is a helper method that provides a dictionary of two data objects that make further data analysis easier
def rsvp_helper(rsvp_data: []):
    rsvp_dates = {}
    rsvp_countries = []
    for rsvp in rsvp_data:
        # Create a dictionary of time & event_url for easier subsequent parsing
        rsvp_dates[rsvp['event']['time']] = rsvp['event']['event_url']
        # Will use this list/array in order to get top 3 country count
        rsvp_countries.append(rsvp['group']['group_country'])
    return {'rsvp_dates': rsvp_dates, 'rsvp_countries': rsvp_countries}


def get_rsvp_count(rsvp_data: []):
    return len(rsvp_data)


def furthest_into_future(rsvp_dates: []):
    potential_dates = rsvp_dates.keys()
    furthest_away_unix = max(potential_dates)
    furthest_away = dt.datetime.utcfromtimestamp(furthest_away_unix/1000).strftime('%Y-%m-%d %H:%M')
    return f'{furthest_away},{rsvp_dates[furthest_away_unix]}'


def most_popular_locations(rsvp_countries: []):
    most_popular_dict = {i: rsvp_countries.count(i) for i in rsvp_countries}
    most_popular_dict = sorted(most_popular_dict.items(), key=lambda x: x[1], reverse=True)
    if len(rsvp_countries) >= 3:
        return [most_popular_dict[0], most_popular_dict[1], most_popular_dict[2]]
    # Handle edge cases where we receive less than 3 total events
    else:
        return [most_popular_dict]


def solution():
    data = get_data_stream()
    rsvp_helper_data = rsvp_helper(data)
    total_count = get_rsvp_count(data)
    future = furthest_into_future(rsvp_helper_data['rsvp_dates'])
    top_countries = most_popular_locations(rsvp_helper_data['rsvp_countries'])
    # Convert top countries to string and manipulate to fit solutions expected formatting
    top_countries_str = ''
    for k, v in top_countries:
        top_countries_str += f'{k}, {v}, '
    top_countries_str = top_countries_str.rstrip(", ")
    print(f'{total_count},{future},{top_countries_str.replace(" ", "")}')


if __name__ == "__main__":
    solution()

'''
    Sample output (using default 60 second timeout):
        > C:\Python\python.exe C:/Users/Williams/PycharmProjects/please-respond/RsvpSorter.py
        > 66,2022-01-22 23:00,https://www.meetup.com/chicago-scuba-meetup/events/276904871/,us,43,gb,6,jp,3
'''
